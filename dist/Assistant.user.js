// ==UserScript==
// @name        Assistant
// @namespace   Assistant
// @version     2.0
// @description WiP
// 
// @include     https://*.popmundo.com/*
//
// @run-at document-end
// @noframes
//
// @grant   GM_getValue
// @grant   GM_setValue
// @grant	GM_getResourceText
// @grant   GM_addStyle
// @grant   GM_deleteValue
// 
// ==/UserScript==

class Assistant
{
	constructor(characterID)
	{
		this.start = Date.now();

		if (characterID == null) {
			this.debugMessage("Cannot start Assistant because no (valid) ID was passed to the constructor");
			return;
		}

		this.characterID = characterID;
		this.debugMessage("Constructing Assistant with CharacterID " + this.characterID);

		this.tickInterval = 1000;
		this.tickCount = 0;
		this.isBusy = false;

		// Collections
		this.characterCollection = {};
		this.relationCollection = {};
		this.modules = {};
		this.parsers = {};
		
		// Load data
		this.repository = new Repository();
		this.load();

		// Add own character as a tracked character
		this.characterCollection.addTrackReason(this.characterID, "self");

		// Save data when window unloads
		window.onbeforeunload = () => {
			this.save();
		};
	}

	debugMessage(message)
	{
		let msPassed = Date.now() - this.start;
		console.log("[" + msPassed.toString() + "][DEBUG] " + message);
	}
	
	load()
	{
		this.settings = this.repository.getValue("settings", {});
		
		this.characterCollection = new CharacterCollection();
		this.characterCollection.load(this.repository);

		this.relationCollection = new RelationCollection(this.characterID);
		this.relationCollection.load(this.repository);
	}
	
	save()
	{
		this.characterCollection.save(this.repository);
		this.relationCollection.save(this.repository);
	}
	
	run()
	{
		this.tickCount++;
		
		if (this.tickCount === 1) {
			// Run parsers on the first tick after page load
			this.runParsers(document);
		}

		// Run modules on every tick
		this.runModules();

		setTimeout(() => {this.run()}, this.tickInterval);
	}
	
	runParsers(context)
	{
		this.debugMessage("Running parsers");
		let start = Date.now();
		let completedParsers = [];
		let failedParsers = [];

		Object.keys(this.parsers).forEach((key) => {
			try {
				let parser = this.parsers[key];
				if (parser.parse(this, context)) {
					completedParsers.push(key);
				} else {
					failedParsers.push(key);
				}
			} catch (exception) {
				this.debugMessage("Error while attempting to run parser \"" + key + "\": " + exception);
				failedParsers.push(key);
			}
		});

		this.debugMessage("Finished running parsers in " + (Date.now() - start) + "ms");
		this.debugMessage("Failed parsers: " + failedParsers.join(', '));
		this.debugMessage("Successful parsers: " + completedParsers.join(', '));
	}

	runModules()
	{
		Object.keys(this.modules).forEach((key) => {
			try {
				let module = this.modules[key];
				module.run(this);
			} catch (exception) {
				this.debugMessage("Error while attempting to run module \"" + key + "\": " + exception);
			}
		});
	}

	addParser(parser)
	{
		if (!parser.hasOwnProperty("name")) {
			this.debugMessage("Error while attempting to register a parser: Parser does not have a \"name\" attribute.");
			return;
		}

		this.parsers[parser.name] = parser;

		try {
			this.parsers[parser.name].init(this);
		} catch (e) {
			this.debugMessage("Failed to init parser '" + parser.name + "': " + e);
		}
	}
}

class Repository
{
	getValue(key, defaultVal)
	{
		let loaded = GM_getValue(key, null);

		if (loaded === null) {
			return (typeof defaultVal === "undefined") ? null : defaultVal;
		}

		return this.unserialize(loaded);
	}
	
	setValue(key, val)
	{
		val = this.serialize(val);
		GM_setValue(key, val);
	}

	serialize(val)
	{
		return JSON.stringify(val);
	}

	unserialize(val)
	{
		return JSON.parse(val);
	}
}

class CharacterCollection
{
	constructor()
	{
		this.storageKey = "characters";
		this.tempCharacters = [];
		this.characters = {};
		this.validAttributes = {};
		this.attributeAliases = {};

		this.addAttribute({
			key: '__trackFor',
			name: 'TrackFor',
		});

		this.addAttribute({
			key: 'id',
			name: 'id',
		});

		this.addAttribute({
			key: 'n',
			name: 'name',
		});

		this.addAttribute({
			key: 'a',
			name: 'age',
		});

		this.addAttribute({
			key: 'v',
			name: 'vip',
		});

		this.addAttribute({
			key: 'img',
			name: 'portrait',
		});

		this.addAttribute({
			key: 'c',
			name: 'city',
		});

		this.addAttribute({
			key: 'l',
			name: 'locale',
		});

		this.addAttribute({
			key: 'ln',
			name: 'localeName',
		});
	}

	addAttribute(attr)
	{
		this.validAttributes[attr.key] = attr;
		this.addAttributeAlias(attr.name, attr.key);
	}

	addAttributeAlias(alias, attr)
	{
		this.attributeAliases[alias] = attr;
	}

	addTrackReason(characterID, reason)
	{
		let reasons = this.getCharacterAttribute(characterID, '__trackFor', []);

		if (reasons.indexOf(reason) === -1) {
			// Character is not tracked for this reason yet
			reasons.push(reason);
			this.setCharacterAttribute(characterID, '__trackFor', reasons);
		}
	}

	removeTrackReason(characterID, reason)
	{
		let reasons = this.getCharacterAttribute(characterID, '__trackFor', []);
		let index   = reasons.indexOf(reason);

		if (index !== -1) {
			delete reasons[index];
			this.setCharacterAttribute(characterID, '__trackFor', reasons);
		}
	}

	isTracked(id)
	{
		return (this.hasCharacter(id) && this.getCharacterAttribute(id, '__trackFor', []).length > 0);
	}

	hasCharacter(characterID)
	{
		return this.characters.hasOwnProperty(characterID);
	}

	hasCharacterAttribute(characterID, attribute)
	{
		return this.hasCharacter(characterID) && this.characters[characterID].hasOwnProperty(attribute);
	}

	getCharacterAttribute(characterID, attribute, defaultVal)
	{
		if (typeof defaultVal === "undefined") {
			defaultVal = null;
		}

		if (!this.validAttributes.hasOwnProperty(attribute)) {
			if (this.attributeAliases.hasOwnProperty(attribute)) {
				attribute = this.attributeAliases[attribute];
			} else {
				throw "Attempt to get invalid attribute '" + attribute + "' on a character";
			}
		}

		if (!this.hasCharacterAttribute(characterID, attribute)) {
			return defaultVal;
		}

		return this.characters[characterID][attribute];
	}

	setCharacterAttribute(characterID, attribute, val)
	{
		if (!this.validAttributes.hasOwnProperty(attribute)) {
			if (this.attributeAliases.hasOwnProperty(attribute)) {
				attribute = this.attributeAliases[attribute];
			} else {
				throw "Attempt to set invalid attribute '" + attribute + "' on a character";
			}
		}

		if (!this.hasCharacter(characterID)) {
			this.characters[characterID] = {id:characterID};
			this.tempCharacters.push(characterID);
		}

		// Todo: Fire event
		this.characters[characterID][attribute] = val;
	}

	removeCharacter(characterID)
	{
		if (this.hasCharacter(characterID)) {
			delete this.characters[characterID];
		}
	}

	cleanTempCharacters()
	{
		// Remove all characters that are marked as temporary and do not
		// have at least one __trackFor reason
		this.tempCharacters.forEach((characterID) => {
			if (this.getCharacterAttribute(characterID, '__trackFor', []).length <= 0) {
				this.removeCharacter(characterID);
			}
		});

		this.tempCharacters = [];
	}
	
	load(repository)
	{
		this.characters = repository.getValue(this.storageKey, {});
	}
	
	save(repository)
	{
		this.cleanTempCharacters();
		repository.setValue(this.storageKey, this.characters);
	}
}

class RelationCollection
{
	constructor(characterID)
	{
		this.characterID = characterID;
		this.storageKey = "relations";
		this.relations = {};
		this.validAttributes = {};
		this.attributeAliases = {};

		this.addAttribute({
			key: 'e',
			name: 'emnity',
		});

		this.addAttribute({
			key: 'f',
			name: 'friendship',
		});

		this.addAttribute({
			key: 'r',
			name: 'romance',
		});

		this.addAttributeAlias('emnity', 'e');
		this.addAttributeAlias('friendship', 'f');
		this.addAttributeAlias('romance', 'r');
	}

	addAttribute(attr)
	{
		this.validAttributes[attr.key] = attr;
	}

	addAttributeAlias(alias, attr)
	{
		this.attributeAliases[alias] = attr;
	}

	getRelationAttribute(id, key, defaultVal)
	{
		if (!this.validAttributes.hasOwnProperty(key)) {
			if (this.attributeAliases.hasOwnProperty(key)) {
				key = this.attributeAliases[key];
			} else {
				throw "Attempt to get invalid attribute '" + key + "' on a relationship";
			}
		}

		if (!this.relations.hasOwnProperty(this.characterID) || !this.relations[this.characterID].hasOwnProperty(id) || !this.relations[this.characterID][id].hasOwnProperty(key)) {
			return (typeof defaultVal === 'undefined') ? null : defaultVal;
		}

		return this.relations[this.characterID][id][key];
	}

	setRelationAttribute(id, key, val)
	{
		if (!this.validAttributes.hasOwnProperty(key)) {
			if (this.attributeAliases.hasOwnProperty(key)) {
				key = this.attributeAliases[key];
			} else {
				throw "Attempt to set invalid attribute '" + key + "' on a relationship";
			}
		}

		if (!this.relations.hasOwnProperty(this.characterID)) {
			this.relations[this.characterID] = {};
		}

		if (!this.relations[this.characterID].hasOwnProperty(id)) {
			this.relations[this.characterID][id] = {};
		}

		this.relations[this.characterID][id][key] = val;
	}
	
	load(repository)
	{
		this.relations = repository.getValue(this.storageKey, {});
	}
	
	save(repository)
	{
		repository.setValue(this.storageKey, this.relations);
	}	
}

let RelationsParser = {
	name: "RelationsParser",

	parse: function(assistant, context) {
		if (context.location.href.indexOf("Character/Relations/") === -1) {
			return false;
		}

		let pageCharId = context.querySelector("#ppm-content div.charPresBox div.idHolder").innerText;

		if(pageCharId != assistant.characterID) {
			return false;
		}

		this.parseRelationsTable(assistant, context);
		return true;
	},

	parseRelationsTable: function(assistant, context) {
		let table = context.querySelector("#ppm-content table.data");
		let rows  = table.querySelectorAll("tbody tr");

		rows.forEach((row) => {
			let cells = row.querySelectorAll("td");

			// Cell 0: Name, ID
			let anchor = cells[0].querySelector("a");
			let name   = anchor.innerText;
			let id     = /([0-9]+)$/.exec(anchor.href)[1];

			// Cell 1: FriendshipTotal
			let friendshipTotal = cells[1].querySelector("span.sortkey").innerText;
			
			// Cell 2: RomanceTotal
			let romanceTotal = cells[2].querySelector("span.sortkey").innerText;

			// Cell 3: EmnityTotal
			let emnityTotal = cells[3].querySelector("span.sortkey").innerText;

			assistant.characterCollection.setCharacterAttribute(id, "name", name);
			assistant.characterCollection.addTrackReason(id, "r" + assistant.characterID);

			assistant.relationCollection.setRelationAttribute(id, "f", friendshipTotal);
			assistant.relationCollection.setRelationAttribute(id, "r", romanceTotal);
			assistant.relationCollection.setRelationAttribute(id, "e", emnityTotal);
		});
	}
};

let CharPresBoxParser = {
	name: "CharPresBoxParser",

	init: function(assistant)
	{
		// Attribute to keep track of when the charpresbox
		// for a character was last parsed
		assistant.characterCollection.addAttribute({
			key: 'cpbp',
			name: 'CharPresBox Parsed',
		});
	},

	parse: function(assistant, context) 
	{
		let pageCharId = context.querySelector("#ppm-content div.charPresBox div.idHolder").innerText;

		if(!assistant.characterCollection.isTracked(pageCharId)) {
			return false;
		}

		return this.parsePresBox(assistant, context, pageCharId);
	},

	parsePresBox: function(assistant, context, id) 
	{
		let charPresBox = context.querySelector("#ppm-content div.charPresBox");
		if (!charPresBox) {return false;}
		// Name, Age, ?CityID, ?LocaleID, ?LocaleName, Portrait, IsVIP, Statement
		let name = charPresBox.querySelector("h2").innerText;
		let age  = /([0-9]+)/.exec(charPresBox.querySelector("div.characterPresentation p:first-of-type").innerText)[1];
		let isVIP = (charPresBox.querySelector("h2 img[src$='VIPNameStar.png']")) ? 1 : 0;
		let portrait = charPresBox.querySelector("div.avatar").style.backgroundImage.split('"')[1];
		let localeId = null; let localeName = null; let cityId = null;

		try {
			let localeLink = charPresBox.querySelector("a[href*='.aspx/Locale/']");
			localeName = localeLink.innerText;
			localeId = /([0-9]+)$/.exec(localeLink);
			cityId = /([0-9]+)$/.exec(charPresBox.querySelector("a[href*='.aspx/City/']"))[1];
		} catch (e) {
			// Failure to parse locale/city may occur when a character is stealthy
		}

		assistant.characterCollection.setCharacterAttribute(id, 'name', name);
		assistant.characterCollection.setCharacterAttribute(id, 'age', age);
		assistant.characterCollection.setCharacterAttribute(id, 'vip', isVIP);
		assistant.characterCollection.setCharacterAttribute(id, 'portrait', portrait);
		assistant.characterCollection.setCharacterAttribute(id, 'city', cityId);
		assistant.characterCollection.setCharacterAttribute(id, 'locale', localeId);
		assistant.characterCollection.setCharacterAttribute(id, 'localeName', localeName);
		assistant.characterCollection.setCharacterAttribute(id, 'cpbp', Date.now());
		return true;
	}
};

let RelationDetailsParser = {
	name: "RelationDetailsParser",

	init: function(assistant) {},

	parse: function(assistant, context)
	{
		let romanceSelf, romanceOther, friendshipSelf, friendshipOther, emnitySelf, emnityOther,
			romance, friendship, emnity, attitudeSelf, attitudeOTher,
			lastInteractionRecieved, lastInteractionPerformed;

	}
}

// Active character ID must be parsed before Assistant can be initiated
let activeCharacter = null;

if (document.location.href.indexOf("ChooseCharacter.aspx") !== -1) {
	let boxes = document.body.querySelectorAll("#ppm-content div.box");
	let characterMap = {};

	boxes.forEach((box) => {
		let name = btoa(box.querySelector("h2 a").innerText);
		let id = box.querySelector("div.idHolder").innerText;

		characterMap[name] = id;
	});

	GM_setValue("characterMap", JSON.stringify(characterMap));
} else {
	// Not the character selection page; characterMap must be set for it to be possible
	// to determine the active character.
	let characterMap = GM_getValue("characterMap", null);
	characterMap = (characterMap === null) ? {} : JSON.parse(characterMap);

	let keysInCharacterMap = Object.keys(characterMap).length;

	try {
		let dropdown = document.querySelector("#character-tools select");
		let options  = dropdown.querySelectorAll("option");
		let selectedOption = dropdown.querySelector("option[selected]");
		let selectedName = btoa(selectedOption.innerText);

		if ((options.length - 1) != keysInCharacterMap) {
			throw "CharacterMap Length Mismatch";
		}

		if (!characterMap.hasOwnProperty(selectedName)) {
			throw "Selected character does not appear in CharacterMap";
		}

		activeCharacter = characterMap[selectedName];
	} catch (e) {
		throw "Failed to parse SelectedCharacter: " + e;
	}
}

let app = new Assistant(activeCharacter);
app.addParser(RelationsParser);
app.addParser(CharPresBoxParser);
app.addParser(RelationDetailsParser);
app.run();